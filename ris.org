#+TITLE: Ruamrudee International School
#+AUTHOR: Jonathan A. Bennett <doulos05@gmail.com>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="styles/readtheorg/css/readtheorg.css"/>

#+HTML_HEAD: <script type="text/javascript" src="styles/lib/js/jquery.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="styles/lib/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="styles/lib/js/jquery.stickytableheaders.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="styles/readtheorg/js/readtheorg.js"></script>

* Quick Summary
- Job Title :: MS Computer Science Teacher
- Dates of Employment :: Aug 2018 to present
- Grades and Courses Taught :: Computer Science 6-8, Intro to Computer Programming (HS), Design Technology (MS & HS), Digital Skills 6-8, Information Technology (HS), Robotics 6-8
* CS Program Development
When I came here, there were no technology classes in the Middle School. There was a developing HS program, but the teacher found the students lacked the foundational skills needed to succeed in the test prep courses. Over the course of the next three years, I developed 3 different classes at each of the Middle School grade levels. All of these courses were 1 semester courses. Some of them met every other day, others met every 4 days.

** Computer Science
The Computer Science classes in the Middle school program are taught using Python and JavaScript and have two primary objectives. First, our students will be prepared for any High School CS classes they choose to take. Second, our students will have the skills needed to build their own programs if they want to.

We achieve the first objective by covering the CSTA standards for Computer Science in our classes. We also teach the foundational concepts and vocabulary such as functions, variables, and control structures.

We acheive the second goal by carefully highlighting the standards which tie our program together. These classes focus most heavily on two specific standards in the CSTA standards: Top Down Design and Debugging. While all of the classes teach additional standards, these two standards form the spine of the curriculum. We selected these standards because they represent the most difficult aspects of creating a computer program on your own.
*** Top Down Design
Top Down Design is the process of taking a large program and breaking it into smaller and easier to solve sub-problems, then connecting the solutions of those sub-problems together to create a solution to the larger problem. In short, the concept of "divide and conquer" applied to computer programming.

Breaking problems into smaller sub-problems is an essential skill if you wish to create programs on your own. If you cannot do that, it becomes incredibly difficult to figure out where you should start working on the problem. Our curriculum in the Middle School spends considerable time exercising the skill of identifying the large problem, it's sub-problems, and their sub-problems. We also teach the students to think about how best to define the boundaries of a given problem. These exercises equip our students with the skills needed to design a computer program.
*** Debugging
Debugging is the process of removing defects from software. It is an incredibly challenging task, and according to a variety of studies conducted of professional programmers over the last 40 years, programmers spend 30 to 60% of their time debugging. It is also an incredibly frustrating process for beginners for a wide variety of reasons, some of which are explored in my master's thesis.

Our program teaches debugging through three major streams of delivery. First, we do regular "speed debugging" activities to start the class in which the students are exposed to the most common typos found in student code and asked to spot them. For these, we use Quizizz to deliver the activity and we track the classwide average as a challenge to improve their class's results over time. Second, we engage in group debugging of the teacher's code. This is done to normalize the idea that making mistakes is part of computer programming, and so it is OK if your code has mistakes in it. These debugging sessions focus on bugs where the code appears to work but is incorrect. Most commonly, these kinds of bugs center around failures to define the boundaries of a given problem or sub-problem. Finally, students are asked to debug code they have not written. This is a common activity for professional programmers, but it also is important because it teaches students how to read code and understand what it does before they start changing it.
** Robotics
The second year, I added Robotics courses, which adopted the NGSS Engineering standards for Middle School. This class was structured around the creation and maintenance of an Engineering Notebook, which documented the development and design of their robots over the course of the semester.

We selected the engineering notebook as the primary means of assessment because constructing a robot is not the primary skill taught in our robotics classes. Rather, the skills we focus on in our class are the incremental design of a solution to a challenge, task, or problem. These classes are structured around a series of tasks which the students construct robots to complete. Along the way, they document the intermediate versions of their robot and code on their way to a working solution.

Part of this documentation is identifying the good and bad aspects of how the robot behaved on the challenge table. They then construct a plan to modify their robot and its code in such a way that they preserve the good aspects and minimize or remove the bad aspects. We encapsulate this process as "Build, test, document, reflect".
** Digital Skills
In my third year, I developed a Digital Skills course intended to build the technology skills needed to succeed in a Google Based school. This course has three areas of focus. First, it focuses on teaching the skills needed to maintain all the lines of communication they use on a regular basis to collaborate with peers and communicate with teachers. This includes managing Google Classroom, keeping their Gmail inbox under control, and organizing their Google Drive. The second area of focus is general competence in the Google Suite of software used on a daily basis in the Middle School, including Google Docs and Sheets. The final area of focus is basic troubleshooting skills for maintaining their computers (software and hardware). This ranges from how and when to install software updates to an overview of the parts of a computer. As part of troubleshooting, we also covered password management and the basics of cybersecurity.

* Robotics Team
Alongside our curriculum program, we launched a Robotics Competition team in the Middle School. The competition teams used the mBot platform and competed in Thailand's MakeX Starter competitions in 2019. Despite the restrictions on competitions in 2020, we have maintained our team and are holding intramural competitions until MakeX can resume their competition schedule.

These competitions provide an excellent opportunity for students who are not athletically minded to get exposure to the cooperative skills typically only developed in team sports. While World Scholars and other academic competitions can provide great learning opportunities, the cooperation around the robot itself brings the sorts of skills required in a basketball or football game to bear as the students cooperate to develop a piece of the whole robot, communicating about their skills, expectations, limitations, and requirements during the building stages.

Then on the day of the competition, they are paired with randomly chosen teams from the competition to tackle the challenges of the competition. This pairing forces students to exercise all those communication skills gained building their own robot to communicate with the new team about how they will move around the field and accomplish the shared objectives.

These robotics teams served as a recruitment tool for 5th graders as they looked forward to Middle School, and there was even a strong push to bring the competition teams down into the ES, though that was not possible because of the timing of the team meetings.
* Focus Block Attendance Project
RIS implements a daily 45 minute block for students to engage in their own projects. Student locations during this time are fluid as the students frequently move to specialist locations to use the 3D printer, access a subject matter expert, or conference with partners working on similar projects. Additionally, there is a content support program that runs during the same time. Students join this content support block if they feel they need additional support with a specific concept in one of their classes or if they need to work on their learning plan before a reassessment opportunity. This fluidity does not work well with PowerSchool, where we track attendance.

In order to provide consistent tracking while we pursued a permanent system, I created a temporary solution for the school. Over the course of the weekend before we started allowing students to move from their primary mentor's room, I built a webpage using Python 3 and Django, hosted for free on PythonAnywhere, which provided a way for teachers to check attendance for their students as well as a way for them to move students to a new room and have the teacher in that room see the student had been moved after a refresh. Finally, an attendance reporting dashboard was built with limited access which allowed the principals to see a list of all students who were marked absent in real time as well as to see the total number of lates and absences over the course of a semester.

The solution was well received by students and teachers alike, who found it easy to navigate and use. Additionally, the team running our Focus Block was able to quickly and easily manage the location of students and teachers and keep track of student numbers in rooms to ensure that they weren't overloaded.

The solution I built was never intended as a permanent solution. Instead, it was designed as an intermediate tool to track student location while our PowerSchool Administrator worked with PowerSchool to learn how to create a plugin which could interface directly with PowerSchool. But as an immediate stopgap, the website gave us a much more systematic way of tracking students during the year required to develop his more permanent solution.
